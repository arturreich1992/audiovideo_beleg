# Setup
### Node.js
Unser Programm basiert auf Node.js. Um die Anwendung ausführen zu können muss die aktuelle Version installiert sein.
Diese lässt sich über die Seite https://nodejs.org/en/ herunterladen.
Mit Node.js wird auch npm installiert. Prüfen Sie, ob Sie beide Programme korrekt installiert haben, indem Sie in die Kommandozeile

    node -v

und 

    npm -v

eingeben. Funktionieren die Befehle nicht, deinstallieren Sie Node.js und laden sich Node.js und npm erneut von https://www.npmjs.com/get-npm herunter.

---
### Git
Das Programm ist über Git verwaltet und hier auf Bitbucket hochgeladen.
Sollte Git auf dem Computer nicht installiert sein, folgen Sie den Anweisungen auf https://git-scm.com/.

Starten Sie die die Kommandozeile und navigieren Sie in den Ordner, wo Sie das Repository einfügen möchten. Führen Sie den nachfolgenden Befehl aus um das Repository herunterzuladen. Für den Download für SSH nutzen Sie 

    git clone git@bitbucket.org:arturreich1992/audiovideo_beleg.git

oder für den Download über HTTPS

    git clone https://bitbucket.org/arturreich1992/audiovideo_beleg.git

---
### Ausführen der Anwendung
Navigieren Sie mit der Kommandozeile in das Verzeichnis *audiovideo_beleg* und führen Sie da den folgenden Befehl aus.

    npm start
Mit diesem Befehl öffnet sich nun ein neues Fenster in Ihrem Browser. Sollte Ihr Standartbrowser Edge oder der InternetExplorer sein, empfehlen wir dringendlich die Installation von [Google Chrome](https://www.google.com/intl/de_ALL/chrome/).
Andere Browser verhindern eventuell die korrekte Ausführung der Anwendung.

Öffnet sich kein neues Fenster, geben Sie in die Adresszeile von Chrome `localhost:8080` ein. Damit verbinden Sie sich auf den lokalen Server, der von der Anwendung bereit gestellt wird.

---
### Weiterführende Einträge
#### [Interaktion mit der Anwendung](https://bitbucket.org/arturreich1992/audiovideo_beleg/wiki/Interaktion%20mit%20der%20Anwendung)
#### [Liste mit Features](https://bitbucket.org/arturreich1992/audiovideo_beleg/wiki/Features)