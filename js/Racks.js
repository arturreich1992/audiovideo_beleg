// default colors
Nexus.colors.fill = "#333"
Nexus.colors.accent = "#FAF9DC"

// create Racks
export var audioPanel1 = new Nexus.Rack("#audioPanel1")
export var audioPanel2 = new Nexus.Rack("#audioPanel2")
export var audioPanel3 = new Nexus.Rack("#audioPanel3")
export var videoPanel1 = new Nexus.Rack("#videoPanel1")
export var videoPanel2 = new Nexus.Rack("#videoPanel2")
export var videoPanel3 = new Nexus.Rack("#videoPanel3")
export var playVideo1 = document.getElementById("playVideo1")
export var playVideo2 = document.getElementById("playVideo2")
export var playVideo3 = document.getElementById("playVideo3")
export var replayVideo1 = document.getElementById("replayVideo1")
export var replayVideo2 = document.getElementById("replayVideo2")

export var playAudio1 = document.getElementById("playAudio1")
export var replayAudio1 = document.getElementById("replayAudio1")
export var playAudio2 = document.getElementById("playAudio2")
export var replayAudio2 = document.getElementById("replayAudio2")
export var playAudio3 = document.getElementById("playAudio3")

export var wiki = document.getElementById("wiki")

/*
Audio 1 - Setup
*/
audioPanel1.speed.mode = "absolute"
audioPanel1.volume.mode = "absolute"
audioPanel1.filter.mode = "absolute"
audioPanel1.noiseConvolver.mode = "absolute"
audioPanel1.echoFilter.mode = "absolute"
audioPanel1.bitcrusherFilter.mode = "absolute"
audioPanel1.echofreq.mode = "absolute"

audioPanel1.volumeNumber.colorize("fill", "#848484")
audioPanel1.speedNumber.colorize("fill", "#848484")
audioPanel1.echofreqNumber.colorize("fill", "#848484")

audioPanel1.volume.min = 0
audioPanel1.volume.max = 100
audioPanel1.volume.step = 1
audioPanel1.volume.value = 50

audioPanel1.speed.min = 0.1
audioPanel1.speed.max = 2
audioPanel1.speed.step = 0.1
audioPanel1.speed.value = 1

audioPanel1.echofreq.min = 0.0
audioPanel1.echofreq.max = 1.0
audioPanel1.echofreq.step = 0.1
audioPanel1.echofreq.value = 0.5


// Numberboxen und Slider immer am Ende linken
audioPanel1.volumeNumber.link(audioPanel1.volume)
audioPanel1.speedNumber.link(audioPanel1.speed)
audioPanel1.echofreqNumber.link(audioPanel1.echofreq)

/*
Audio 2 - Setup
*/
audioPanel2.speed.mode = "absolute"
audioPanel2.volume.mode = "absolute"
audioPanel2.filter.mode = "absolute"
audioPanel2.noiseConvolver.mode = "absolute"
audioPanel2.echoFilter.mode = "absolute"
audioPanel2.bitcrusherFilter.mode = "absolute"
audioPanel2.echofreq.mode = "absolute"

audioPanel2.volumeNumber.colorize("fill", "#848484")
audioPanel2.speedNumber.colorize("fill", "#848484")
audioPanel2.echofreqNumber.colorize("fill", "#848484")

audioPanel2.volume.min = 0
audioPanel2.volume.max = 100
audioPanel2.volume.step = 0.5
audioPanel2.volume.value = 50

audioPanel2.speed.min = 0.1
audioPanel2.speed.max = 2
audioPanel2.speed.step = 0.1
audioPanel2.speed.value = 1

audioPanel2.echofreq.min = 0.0
audioPanel2.echofreq.max = 1.0
audioPanel2.echofreq.step = 0.1
audioPanel2.echofreq.value = 0.5

// Numberboxen und Slider immer am Ende linken
audioPanel2.volumeNumber.link(audioPanel2.volume)
audioPanel2.speedNumber.link(audioPanel2.speed)
audioPanel2.echofreqNumber.link(audioPanel2.echofreq)

/*
Audio 3 - Setup
*/
audioPanel3.crossfader.mode = "absolute"

audioPanel3.crossvalue.colorize("fill", "#848484")

audioPanel3.crossfader.min = 0
audioPanel3.crossfader.max = 100
audioPanel3.crossfader.step = 0.5
audioPanel3.crossfader.value = 50

// Numberboxen und Slider immer am Ende linken
audioPanel3.crossvalue.link(audioPanel3.crossfader)

// Biquad
audioPanel1.highpass.mode = "absolute"
audioPanel1.notch.mode = "absolute"
audioPanel1.peaking.mode = "absolute"
audioPanel1.lowpass.mode = "absolute"
audioPanel1.highshelf.mode = "absolute"
audioPanel1.lowshelf.mode = "absolute"

audioPanel1.gain.mode = "absolute"
audioPanel1.quality.mode = "absolute"

audioPanel1.highpass.max = 20000
audioPanel1.notch.max = 20000
audioPanel1.peaking.max = 20000
audioPanel1.lowpass.max = 20000
audioPanel1.highshelf.max = 20000
audioPanel1.lowshelf.max = 20000

audioPanel1.highpass.step = 1
audioPanel1.notch.step = 1
audioPanel1.peaking.step = 1
audioPanel1.lowpass.step = 1
audioPanel1.highshelf.step = 1
audioPanel1.lowshelf.step = 1

audioPanel1.highpass.value = 440
audioPanel1.notch.value = 440
audioPanel1.peaking.value = 440
audioPanel1.lowpass.value = 440
audioPanel1.highshelf.value = 440
audioPanel1.lowshelf.value = 440

audioPanel1.gain.min = -40.0
audioPanel1.gain.max = 40.0
audioPanel1.gain.step = 1
audioPanel1.gain.value = 0.0

audioPanel1.quality.min = 0.0001
audioPanel1.quality.max = 1000.0
audioPanel1.quality.step = 0.0001
audioPanel1.quality.value = 1.0

audioPanel2.highpass.mode = "absolute"
audioPanel2.notch.mode = "absolute"
audioPanel2.peaking.mode = "absolute"
audioPanel2.lowpass.mode = "absolute"
audioPanel2.highshelf.mode = "absolute"
audioPanel2.lowshelf.mode = "absolute"

audioPanel2.gain.mode = "absolute"
audioPanel2.quality.mode = "absolute"

audioPanel2.highpass.max = 20000
audioPanel2.notch.max = 20000
audioPanel2.peaking.max = 20000
audioPanel2.lowpass.max = 20000
audioPanel2.highshelf.max = 20000
audioPanel2.lowshelf.max = 20000

audioPanel2.highpass.step = 1
audioPanel2.notch.step = 1
audioPanel2.peaking.step = 1
audioPanel2.lowpass.step = 1
audioPanel2.highshelf.step = 1
audioPanel2.lowshelf.step = 1

audioPanel2.highpass.value = 440
audioPanel2.notch.value = 440
audioPanel2.peaking.value = 440
audioPanel2.lowpass.value = 440
audioPanel2.highshelf.value = 440
audioPanel2.lowshelf.value = 440

audioPanel2.gain.min = -40.0
audioPanel2.gain.max = 40.0
audioPanel2.gain.step = 1
audioPanel2.gain.value = 0.0

audioPanel2.quality.min = 0.0001
audioPanel2.quality.max = 1000.0
audioPanel2.quality.step = 0.0001
audioPanel2.quality.value = 1

videoPanel1.volume.mode = "absolute"
videoPanel1.volume.min = 0;
videoPanel1.volume.max = 100;
videoPanel1.volume.step = 1;
videoPanel1.volume.value = 50;

videoPanel2.volume.mode = "absolute"
videoPanel2.volume.min = 0;
videoPanel2.volume.max = 100;
videoPanel2.volume.step = 1;
videoPanel2.volume.value = 50;

videoPanel1.varianz.mode = "absolute"
videoPanel1.varianz.min = 0;
videoPanel1.varianz.max = 400;
videoPanel1.varianz.step = 1;
videoPanel1.varianz.value = 100;

videoPanel2.varianz.mode = "absolute"
videoPanel2.varianz.min = 0;
videoPanel2.varianz.max = 400;
videoPanel2.varianz.step = 1;
videoPanel2.varianz.value = 100;

videoPanel1.stackblurradius.mode = "absolute"
videoPanel1.stackblurradius.min = 0;
videoPanel1.stackblurradius.max = 180;
videoPanel1.stackblurradius.step = 1;
videoPanel1.stackblurradius.value = 4;

videoPanel2.stackblurradius.mode = "absolute"
videoPanel2.stackblurradius.min = 0;
videoPanel2.stackblurradius.max = 180;
videoPanel2.stackblurradius.step = 1;
videoPanel2.stackblurradius.value = 4;

import VideoPlayer from "./videoPlayer.js"
export var video1 = document.getElementById("video1")
export var video2 = document.getElementById("video2")

export var videoPlayer1 = new VideoPlayer(video1, document.getElementById("c1"), document.getElementById("c2"))
export var videoPlayer2 = new VideoPlayer(video2, document.getElementById("c3"), document.getElementById("c4"))

export var videoVolume1 = videoPanel1.volume
export var videoVolume2 = videoPanel2.volume
export var chromaKeyToggle1 = videoPanel1.chromakey
export var chromaKeyToggle2 = videoPanel2.chromakey
export var chromaDifference1 = videoPanel1.varianz
export var chromaDifference2 = videoPanel2.varianz
export var invertToggle1 = videoPanel1.invert
export var invertToggle2 = videoPanel2.invert
export var stackBlurToggle1 = videoPanel1.stackblur
export var stackBlurToggle2 = videoPanel2.stackblur
export var stackBlurRadius1 = videoPanel1.stackblurradius
export var stackBlurRadius2 = videoPanel2.stackblurradius
export var greyScaleToggle1 = videoPanel1.greyscale
export var greyScaleToggle2 = videoPanel2.greyscale

var chromaColorVideo1 = [0, 0, 0];
AColorPicker.from(document.getElementById("colorPicker1"))
    .on('change', (picker, color) => {
        document.getElementById("selectColor1").style.backgroundColor = color;
        chromaColorVideo1 = AColorPicker.parseColor(color, "rgb");
        videoPlayer1.setColors(chromaColorVideo1);
    });

var chromaColorVideo2 = [0, 0, 0];
AColorPicker.from(document.getElementById("colorPicker2"))
    .on('change', (picker, color) => {
        document.getElementById("selectColor2").style.backgroundColor = color;
        chromaColorVideo2 = AColorPicker.parseColor(color, "rgb");
        videoPlayer2.setColors(chromaColorVideo2);
    });

document.getElementById("audio1Title").innerText = "beat_1.mp3";
document.getElementById("audio2Title").innerText = "beat_2.mp3";
document.getElementById("video1Title").innerText = "explosion.mpd";
document.getElementById("video2Title").innerText = "sintel.mpd";