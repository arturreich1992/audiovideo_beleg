import Player from './player.js';

//variables for both of the canvas waveform visualiser
var canvas, ctx, analyser, fbc_array, bars, bar_x, bar_width, bar_height;
var canvas2, ctx2, analyser2, fbc_array2, bars2, bar_x2, bar_width2, bar_height2;

var audioContext = new AudioContext();

var player = new Player('beat_1.mp3', audioContext);
var player2 = new Player('beat_2.mp3', audioContext);

var crossFade1 = 0.5;
var crossFade2 = 0.5;


export function changeAudio(panel, audio) {
    if (panel == 1) {
        document.getElementById('playAudio1').innerHTML = '<span class="glyphicon glyphicon-play" aria-hidden="true"></span>';
        player.audio.audioFile.pause();
        player.isPlaying = false;
        player.audio = player.createSource(audio, audioContext);
        initFrequenzAnalyser(panel);

    } else {
        document.getElementById('playAudio2').innerHTML = '<span class="glyphicon glyphicon-play" aria-hidden="true"></span>';
        player2.audio.audioFile.pause();
        player2.isPlaying = false;
        player2.audio = player2.createSource(audio, audioContext);
        initFrequenzAnalyser(panel);
    }
}

export function changeSpeed(audio, value) {
    if (audio == 'audio1') {
        player.audio.audioFile.playbackRate = value;
    }

    if (audio == 'audio2') {
        player2.audio.audioFile.playbackRate = value;
    }
}

export function changeVolume(audio, value) {
    if (audio == 'audio1') {
        player.audio.gainNode.gain.value = value / 100 * crossFade1;

    }

    if (audio == 'audio2') {
        player2.audio.gainNode.gain.value = value / 100 * crossFade2;
    }

}

export function toggleFilterOn(audio, value) {
    if (audio == 'audio1') {
        //Alle Verbindungen trennen
        player.audio.source.disconnect();
        player.audio.gainNode.disconnect();

        //Neue Verbindung mit source -> gainNode -> filter -> destination
        player.audio.source.connect(player.audio.gainNode);
        player.audio.gainNode.connect(player.audio.filter);

        player.audio.filter.connect(analyser);
        player.audio.filter.connect(audioContext.destination);
    }

    if (audio == 'audio2') {
        //Alle Verbindungen trennen
        player2.audio.source.disconnect();
        player2.audio.gainNode.disconnect();

        //Neue Verbindung mit source -> gainNode -> filter -> destination
        player2.audio.source.connect(player2.audio.gainNode);
        player2.audio.gainNode.connect(player2.audio.filter);

        player2.audio.filter.connect(analyser2);
        player2.audio.filter.connect(audioContext.destination);
    }
}

export function toggleFilterOff(audio, value) {
    if (audio == 'audio1') {
        player.audio.filter.disconnect();
        player.audio.source.connect(player.audio.gainNode);
        player.audio.gainNode.connect(audioContext.destination);
        player.audio.source.connect(analyser);
    }

    if (audio == 'audio2') {
        player2.audio.filter.disconnect();
        player2.audio.source.connect(player2.audio.gainNode);
        player2.audio.gainNode.connect(audioContext.destination);
        player2.audio.source.connect(analyser2);
    }
}

export function setFilterFreq(audio, value) {
    if (audio == 'audio1') {
        player.audio.filter.frequency.value = value;
    }

    if (audio == 'audio2') {
        player2.audio.filter.frequency.value = value;
    }
}

export function setFilter(audio, filter, value) {
    if (audio == 'audio1') {
        switch (filter) {
            case 0:
                player.audio.filter.type = 'highpass';
                player.audio.filter.frequency.value = value;
                break;
            case 1:
                player.audio.filter.type = 'notch';
                player.audio.filter.frequency.value = value;
                break;
            case 2:
                player.audio.filter.type = 'peaking';
                player.audio.filter.frequency.value = value;
                break;
            case 3:
                player.audio.filter.type = 'lowpass';
                player.audio.filter.frequency.value = value;
                break;
            case 4:
                player.audio.filter.type = 'highshelf';
                player.audio.filter.frequency.value = value;
                break;

            case 5:
                player.audio.filter.type = 'lowshelf';
                player.audio.filter.frequency.value = value;
                break;
        }

    }

    if (audio == 'audio2') {
        switch (filter) {
            case 0:
                player2.audio.filter.type = 'highpass';
                player2.audio.filter.frequency.value = value;
                break;
            case 1:
                player2.audio.filter.type = 'notch';
                player2.audio.filter.frequency.value = value;
                break;
            case 2:
                player2.audio.filter.type = 'peaking';
                player2.audio.filter.frequency.value = value;
                break;
            case 3:
                player2.audio.filter.type = 'lowpass';
                player2.audio.filter.frequency.value = value;
                break;
            case 4:
                player2.audio.filter.type = 'highshelf';
                player2.audio.filter.frequency.value = value;
                break;
            case 5:
                player2.audio.filter.type = 'lowshelf';
                player2.audio.filter.frequency.value = value;
                break;
        }
    }
}

export function setFilterQuality(audio, value) {
    if (audio == 'audio1') {
        player.audio.filter.Q.value = value;
    }

    if (audio == 'audio2') {
        player2.audio.filter.Q.value = value;
    }
}

export function setFilterGain(audio, value) {
    if (audio == 'audio1') {
        player.audio.filter.gain.value = value;
    }

    if (audio == 'audio2') {
        player2.audio.filter.gain.value = value;
    }
}

export function toggleOwnFilterOn(audio, filter) {
    if (audio == 'audio1') {
        addFilter(player, filter, 1);
    } else if (audio == 'audio2') {
        addFilter(player2, filter, 2);
    }
}

export function toggleOwnFilterOff(audio, filter) {
    if (audio == 'audio1') {
        disconnectFilter(player, filter, 1);
    } else if (audio == 'audio2') {
        disconnectFilter(player2, filter, 2)
    }
}

function addFilter(player1, filter, panel) {
    player1.audio.source.disconnect();
    player1.audio.gainNode.disconnect();

    switch (filter) {
        case "noiseConvolver":
            var left = player1.audio.noiseBuffer.getChannelData(0);
            var right = player1.audio.noiseBuffer.getChannelData(1);

            for (var i = 0; i < player1.audio.noiseBuffer.length; i++) {
                left[i] = Math.random() * 2 - 1;
                right[i] = Math.random() * 2 - 1;
            }

            player1.audio.noiseConvolver.buffer = player1.audio.noiseBuffer;

            player1.audio.source.connect(player1.audio.gainNode);
            player1.audio.gainNode.connect(player1.audio.noiseConvolver);
            player1.audio.noiseConvolver.connect(audioContext.destination);
            panel == 1 ? player1.audio.noiseConvolver.connect(analyser) : player1.audio.noiseConvolver.connect(analyser2);

            break;
        case "echoFilter":
            player1.audio.source.connect(player1.audio.gainNode);
            player1.audio.gainNode.connect(player1.audio.echoFilter);
            player1.audio.gainNode.connect(audioContext.destination);
            player1.audio.echoFilter.connect(audioContext.destination);
            panel == 1 ? player1.audio.echoFilter.connect(analyser) : player1.audio.echoFilter.connect(analyser2);
            break;
        case "bitcrusherFilter":
            player1.audio.bitcrushFilter.bits = 4;
            player1.audio.bitcrushFilter.normfreq = 0.1;
            var step = Math.pow(1 / 2, player1.audio.bitcrushFilter.bits);
            var phaser = 0;
            var last = 0;

            player1.audio.bitcrushFilter.onaudioprocess = function(e) {
                var input = e.inputBuffer.getChannelData(0);
                var output = e.outputBuffer.getChannelData(0);

                for (var i = 0; i < player1.audio.bitcrushFilter.bufferSize; i++) {
                    phaser += player1.audio.bitcrushFilter.normfreq;

                    if (phaser >= 1.0) {
                        phaser -= 1.0;
                        last = step * Math.floor(input[i] / step + 0.5);
                    }
                    output[i] = last;
                }
            };

            player1.audio.source.connect(player1.audio.gainNode);
            player1.audio.gainNode.connect(player1.audio.bitcrushFilter);
            player1.audio.bitcrushFilter.connect(audioContext.destination);
            panel == 1 ? player1.audio.bitcrushFilter.connect(analyser) : player1.audio.bitcrushFilter.connect(analyser2);
            break;
    }
}

function disconnectFilter(player1, filter, panel) {
    switch (filter) {
        case "noiseConvolver":
            player1.audio.noiseConvolver.disconnect();
            player1.audio.source.connect(player1.audio.gainNode);
            player1.audio.gainNode.connect(audioContext.destination);
            panel == 1 ? player1.audio.gainNode.connect(analyser) : player1.audio.gainNode.connect(analyser2);
            break;
        case "echoFilter":
            player1.audio.echoFilter.disconnect();
            player1.audio.source.connect(player1.audio.gainNode);
            player1.audio.gainNode.connect(audioContext.destination);
            panel == 1 ? player1.audio.gainNode.connect(analyser) : player1.audio.gainNode.connect(analyser2);
            break;
        case "bitcrusherFilter":
            player1.audio.bitcrushFilter.disconnect();
            player1.audio.source.connect(player1.audio.gainNode);
            player1.audio.gainNode.connect(audioContext.destination);
            panel == 1 ? player1.audio.gainNode.connect(analyser) : player1.audio.gainNode.connect(analyser2);
            break;
    }
}

export function setEchoFilterFreq(audio, value) {
    if (audio == 'audio1') {
        player.audio.echoFilter.delayTime.value = value;
    }

    if (audio == 'audio2') {
        player2.audio.echoFilter.delayTime.value = value;
    }
}

export function playPause1() {
    if (!player.isPlaying) {
        document.getElementById('playAudio1').innerHTML = '<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>';
        player.audio.audioFile.play();
        player.isPlaying = true;
        initFrequenzAnalyser(1);
    } else {
        document.getElementById('playAudio1').innerHTML = '<span class="glyphicon glyphicon-play" aria-hidden="true"></span>';
        player.audio.audioFile.pause();
        player.isPlaying = false;
    }
}

export function playPause2() {
    if (!player2.isPlaying) {
        document.getElementById('playAudio2').innerHTML = '<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>';
        player2.audio.audioFile.play();
        player2.isPlaying = true;
        initFrequenzAnalyser(2);
    } else {
        document.getElementById('playAudio2').innerHTML = '<span class="glyphicon glyphicon-play" aria-hidden="true"></span>';
        player2.audio.audioFile.pause();
        player2.isPlaying = false;
    }
}

export function replay1() {
    if (!player.isLooping) {
        player.audio.audioFile.loop = true;
        player.isLooping = true;
        document.getElementById("replayAudio1").style.backgroundColor = "#c6c7a4";
    } else {
        player.audio.audioFile.loop = false;
        player.isLooping = false;
        document.getElementById("replayAudio1").style.backgroundColor = "#FAF9DC";
    }
}

export function replay2() {
    if (!player2.isLooping) {
        player2.audio.audioFile.loop = true;
        player2.isLooping = true;
        document.getElementById("replayAudio2").style.backgroundColor = "#c6c7a4";
    } else {
        player2.audio.audioFile.loop = false;
        player2.isLooping = false;
        document.getElementById("replayAudio2").style.backgroundColor = "#FAF9DC";
    }
}

export function crossFading(value, limitOne, limitTwo) {
    var x = parseInt(value) / 100;
    var volumeLimitOne = limitOne / 100;
    var volumeLimitTwo = limitTwo / 100;

    var gain1 = Math.cos(x * 0.5 * Math.PI);
    var gain2 = Math.cos((1.0 - x) * 0.5 * Math.PI);
    crossFade1 = gain1;
    crossFade2 = gain2;
    player.audio.gainNode.gain.value = gain1 * volumeLimitOne;
    player2.audio.gainNode.gain.value = gain2 * volumeLimitTwo;
}

export function initFrequenzAnalyser(panel) {
    if (panel == 1) {
        analyser = audioContext.createAnalyser();
        canvas = document.getElementById('fileAnalyser1');
        ctx = canvas.getContext('2d');
        player.audio.gainNode.connect(analyser);

    } else if (panel == 2) {
        analyser2 = audioContext.createAnalyser();
        canvas2 = document.getElementById('fileAnalyser2');
        ctx2 = canvas2.getContext('2d');
        player2.audio.gainNode.connect(analyser2);
    }

    frameLooper();
}

function frameLooper() {
    window.requestAnimationFrame(frameLooper);
    if (analyser != undefined) {
        fbc_array = new Uint8Array(analyser.frequencyBinCount);
        analyser.getByteFrequencyData(fbc_array);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = '#00CCFF';
        bars = 100;

        for (var i = 0; i < bars; i++) {
            bar_x = i * 3;
            bar_width = 2;
            bar_height = -(fbc_array[i] / 2);
            ctx.fillRect(bar_x, canvas.height, bar_width, bar_height);
        }
    }

    if (analyser2 != undefined) {
        fbc_array2 = new Uint8Array(analyser2.frequencyBinCount);
        analyser2.getByteFrequencyData(fbc_array2);
        ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
        ctx2.fillStyle = '#FFCC00';
        bars2 = 100;

        for (var i = 0; i < bars2; i++) {
            bar_x2 = i * 3;
            bar_width2 = 2;
            bar_height2 = -(fbc_array2[i] / 2);
            ctx2.fillRect(bar_x2, canvas2.height, bar_width2, bar_height2);
        }
    }
}