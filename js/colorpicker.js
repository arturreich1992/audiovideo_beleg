function colorpickerHide(picker) {
    var x = document.getElementById(picker);
    if (x.style.display == "none") {
        x.style.display = "inline-block";
    } else {
        x.style.display = "none";
    }
}