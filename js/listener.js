import * as elements from "./Racks.js";
import * as effects from './audioEffects.js';

window.addEventListener("load", effects.initFrequenzAnalyser, false);

elements.playAudio1.addEventListener("click", effects.playPause1);

elements.playAudio2.addEventListener("click", effects.playPause2);

elements.replayAudio1.addEventListener("click", effects.replay1);
elements.replayAudio2.addEventListener("click", effects.replay2);



elements.audioPanel1.volume.on('change', function(v) {
    effects.changeVolume('audio1', v);
});

elements.audioPanel1.speed.on('change', function(v) {
    effects.changeSpeed('audio1', v);
});


elements.audioPanel1.filter.on('change', function(v) {
    if (elements.audioPanel1.filter.state && (!elements.audioPanel1.echoFilter.state && !elements.audioPanel1.noiseConvolver.state && !elements.audioPanel1.bitcrusherFilter.state)) {
        effects.toggleFilterOn('audio1', v);
    } else {
        effects.toggleFilterOff('audio1', v);
    }
});


//Eigene Filter

elements.audioPanel1.noiseConvolver.on('change', function(v) {
    if (elements.audioPanel1.noiseConvolver.state && (!elements.audioPanel1.filter.state && !elements.audioPanel1.echoFilter.state && !elements.audioPanel1.bitcrusherFilter.state)) {
        effects.toggleOwnFilterOn('audio1', 'noiseConvolver');
    } else {
        effects.toggleOwnFilterOff('audio1', 'noiseConvolver');
    }
});

elements.audioPanel1.echoFilter.on('change', function(v) {
    if (elements.audioPanel1.echoFilter.state && (!elements.audioPanel1.filter.state && !elements.audioPanel1.noiseConvolver.state && !elements.audioPanel1.bitcrusherFilter.state)) {
        effects.toggleOwnFilterOn('audio1', 'echoFilter');
    } else {
        effects.toggleOwnFilterOff('audio1', 'echoFilter');
    }
});

elements.audioPanel1.bitcrusherFilter.on('change', function(v) {
    if (elements.audioPanel1.bitcrusherFilter.state && (!elements.audioPanel1.filter.state && !elements.audioPanel1.echoFilter.state && !elements.audioPanel1.noiseConvolver.state)) {
        effects.toggleOwnFilterOn('audio1', 'bitcrusherFilter');
    } else {
        effects.toggleOwnFilterOff('audio1', 'bitcrusherFilter');
    }
});

elements.audioPanel1.echofreq.on('change', function(v) {
    if (elements.audioPanel1.echoFilter.state) {
        effects.setEchoFilterFreq('audio1', v);
    }
});


elements.audioPanel2.speed.on('change', function(v) {
    effects.changeSpeed('audio2', v);
});
elements.audioPanel2.volume.on('change', function(v) {
    effects.changeVolume('audio2', v);
});


elements.audioPanel2.filter.on('change', function(v) {
    if (elements.audioPanel2.filter.state && (!elements.audioPanel2.echoFilter.state && !elements.audioPanel2.noiseConvolver.state && !elements.audioPanel2.bitcrusherFilter.state)) {
        effects.toggleFilterOn('audio2', v);
    } else {
        effects.toggleFilterOff('audio2', v);
    }
});




//Eigene Filter

elements.audioPanel2.noiseConvolver.on('change', function(v) {
    if (elements.audioPanel2.noiseConvolver.state && (!elements.audioPanel2.filter.state && !elements.audioPanel2.echoFilter.state && !elements.audioPanel2.bitcrusherFilter.state)) {
        effects.toggleOwnFilterOn('audio2', 'noiseConvolver');
    } else {
        effects.toggleOwnFilterOff('audio2', 'noiseConvolver');
    }
});

elements.audioPanel2.echoFilter.on('change', function(v) {
    if (elements.audioPanel2.echoFilter.state && (!elements.audioPanel2.filter.state && !elements.audioPanel2.noiseConvolver.state && !elements.audioPanel2.bitcrusherFilter.state)) {
        effects.toggleOwnFilterOn('audio2', 'echoFilter');
    } else {
        effects.toggleOwnFilterOff('audio2', 'echoFilter');
    }
});

elements.audioPanel2.bitcrusherFilter.on('change', function(v) {
    if (elements.audioPanel2.bitcrusherFilter.state && (!elements.audioPanel2.filter.state && !elements.audioPanel2.echoFilter.state && !elements.audioPanel2.noiseConvolver.state)) {
        effects.toggleOwnFilterOn('audio2', 'bitcrusherFilter');
    } else {
        effects.toggleOwnFilterOff('audio2', 'bitcrusherFilter');
    }
});

elements.audioPanel2.echofreq.on('change', function(v) {
    if (elements.audioPanel2.echoFilter.state) {
        effects.setEchoFilterFreq('audio2', v);
    }

});

//AudioPanel3
elements.audioPanel3.crossfader.on('change', function(v) {
    effects.crossFading(v, elements.audioPanel1.volume.value, elements.audioPanel2.volume.value);
});

// Biquad
elements.audioPanel1.highpass.on('change', function(v) {
    if (elements.audioPanel1.filter.state && document.getElementById("highpass1").checked) {
        effects.setFilterFreq('audio1', v);
    }
});

elements.audioPanel1.notch.on('change', function(v) {
    if (elements.audioPanel1.filter.state && document.getElementById("notch1").checked) {
        effects.setFilterFreq('audio1', v);
    }
});

elements.audioPanel1.peaking.on('change', function(v) {
    if (elements.audioPanel1.filter.state && document.getElementById("peaking1").checked) {
        effects.setFilterFreq('audio1', v);
    }
});

elements.audioPanel1.lowpass.on('change', function(v) {
    if (elements.audioPanel1.filter.state && document.getElementById("lowpass1").checked) {
        effects.setFilterFreq('audio1', v);
    }
});

elements.audioPanel1.highshelf.on('change', function(v) {
    if (elements.audioPanel1.filter.state && document.getElementById("highshelf1").checked) {
        effects.setFilterFreq('audio1', v);
    }
});

elements.audioPanel1.lowshelf.on('change', function(v) {
    if (elements.audioPanel1.filter.state && document.getElementById("lowshelf1").checked) {
        effects.setFilterFreq('audio1', v);
    }
});

elements.audioPanel1.gain.on('change', function(v) {
    if (elements.audioPanel1.filter.state) {
        effects.setFilterGain('audio1', v);
    }
});

elements.audioPanel1.quality.on('change', function(v) {
    if (elements.audioPanel1.filter.state) {
        effects.setFilterQuality('audio1', v);
    }
});


elements.audioPanel2.highpass.on('change', function(v) {
    if (elements.audioPanel2.filter.state && document.getElementById("highpass2").checked) {
        effects.setFilterFreq('audio2', v);
    }
});

elements.audioPanel2.notch.on('change', function(v) {
    if (elements.audioPanel2.filter.state && document.getElementById("notch2").checked) {
        effects.setFilterFreq('audio2', v);
    }
});

elements.audioPanel2.peaking.on('change', function(v) {
    if (elements.audioPanel2.filter.state && document.getElementById("peaking2").checked) {
        effects.setFilterFreq('audio2', v);
    }
});

elements.audioPanel2.lowpass.on('change', function(v) {
    if (elements.audioPanel2.filter.state && document.getElementById("lowpass2").checked) {
        effects.setFilterFreq('audio2', v);
    }
});

elements.audioPanel2.highshelf.on('change', function(v) {
    if (elements.audioPanel2.filter.state && document.getElementById("highshelf2").checked) {
        effects.setFilterFreq('audio2', v);
    }
});

elements.audioPanel2.lowshelf.on('change', function(v) {
    if (elements.audioPanel2.filter.state && document.getElementById("lowshelf2").checked) {
        effects.setFilterFreq('audio2', v);
    }
});

elements.audioPanel2.gain.on('change', function(v) {
    if (elements.audioPanel2.filter.state) {
        effects.setFilterGain('audio2', v);
    }
});

elements.audioPanel2.quality.on('change', function(v) {
    if (elements.audioPanel2.filter.state) {
        effects.setFilterQuality('audio2', v);
    }
});

$(document).ready(function() {
    $("#highpass1").change(function() {
        if (elements.audioPanel1.filter.state) {
            effects.setFilter('audio1', 0, elements.audioPanel1.highpass.value);
        }
    });
    $("#notch1").change(function() {
        if (elements.audioPanel1.filter.state) {
            effects.setFilter('audio1', 1, elements.audioPanel1.notch.value);
        }
    });
    $("#peaking1").change(function() {
        if (elements.audioPanel1.filter.state) {
            effects.setFilter('audio1', 2, elements.audioPanel1.peaking.value);
        }
    });
    $("#lowpass1").change(function() {
        if (elements.audioPanel1.filter.state) {
            effects.setFilter('audio1', 3, elements.audioPanel1.lowpass.value);
        }
    });
    $("#highshelf1").change(function() {
        if (elements.audioPanel1.filter.state) {
            effects.setFilter('audio1', 4, elements.audioPanel1.highshelf.value);
        }
    });
    $("#lowshelf1").change(function() {
        if (elements.audioPanel1.filter.state) {
            effects.setFilter('audio1', 5, elements.audioPanel1.lowshelf.value);
        }
    });

    $("#highpass2").change(function() {
        if (elements.audioPanel2.filter.state) {
            effects.setFilter('audio2', 0, elements.audioPanel2.highpass.value);
        }
    });
    $("#notch2").change(function() {
        if (elements.audioPanel2.filter.state) {
            effects.setFilter('audio2', 1, elements.audioPanel2.notch.value);
        }
    });
    $("#peaking2").change(function() {
        if (elements.audioPanel2.filter.state) {
            effects.setFilter('audio2', 2, elements.audioPanel2.peaking.value);
        }
    });
    $("#lowpass2").change(function() {
        if (elements.audioPanel2.filter.state) {
            effects.setFilter('audio2', 3, elements.audioPanel2.lowpass.value);
        }
    });
    $("#highshelf2").change(function() {
        if (elements.audioPanel2.filter.state) {
            effects.setFilter('audio2', 4, elements.audioPanel2.highshelf.value);
        }
    });
    $("#lowshelf2").change(function() {
        if (elements.audioPanel2.filter.state) {
            effects.setFilter('audio2', 5, elements.audioPanel2.lowshelf.value);
        }
    });
});

elements.playVideo1.addEventListener("click", function() {
    playVideo(1);
});
elements.playVideo2.addEventListener("click", function() {
    playVideo(2);
});

elements.replayVideo1.addEventListener("click", function() {
    loopVideo(1);
})

elements.replayVideo2.addEventListener("click", function() {
    loopVideo(2);
})


var stateVideo1 = false;
var stateVideo2 = false;
var loopVideo1 = false;
var loopVideo2 = false;

function playVideo(video) {
    if (video == 1) {
        stateVideo1 == true ? stateVideo1 = false : stateVideo1 = true;
        if (stateVideo1) {
            document.getElementById('playVideo1').innerHTML = '<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>';
            elements.videoPlayer1.video.play();
        } else {
            document.getElementById('playVideo1').innerHTML = '<span class="glyphicon glyphicon-play" aria-hidden="true"></span>';
            elements.videoPlayer1.video.pause();
        }
    }

    if (video == 2) {
        stateVideo2 == true ? stateVideo2 = false : stateVideo2 = true;
        if (stateVideo2) {
            document.getElementById('playVideo2').innerHTML = '<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>';
            elements.videoPlayer2.video.play();
        } else {
            document.getElementById('playVideo2').innerHTML = '<span class="glyphicon glyphicon-play" aria-hidden="true"></span>';
            elements.videoPlayer2.video.pause();
        }
    }
}

function loopVideo(video) {
    if (video == 1) {
        loopVideo1 == true ? loopVideo1 = false : loopVideo1 = true;
        if (loopVideo1) {
            document.getElementById("replayVideo1").style.backgroundColor = "#c6c7a4";
            elements.videoPlayer1.video.loop = true;
        } else {
            document.getElementById("replayVideo1").style.backgroundColor = "#FAF9DC";
            elements.videoPlayer1.video.loop = false;
        }
    }

    if (video == 2) {
        loopVideo2 == true ? loopVideo2 = false : loopVideo2 = true;
        if (loopVideo2) {
            document.getElementById("replayVideo2").style.backgroundColor = "#c6c7a4";
            elements.videoPlayer2.video.loop = true;
        } else {
            document.getElementById("replayVideo2").style.backgroundColor = "#FAF9DC";
            elements.videoPlayer2.video.loop = false;
        }
    }
}

elements.playVideo3.addEventListener("click", drawVideo3);
var stateVideo3 = false;

function drawVideo3() {
    stateVideo3 == true ? stateVideo3 = false : stateVideo3 = true;

    if (stateVideo3 && !elements.videoPlayer1.video.paused || !elements.videoPlayer2.video.paused) {
        document.getElementById("c5").style.visibility = "visible";
        document.getElementById("c6").style.visibility = "visible";
        elements.videoPlayer1.processor.drawOnVideo3 = true;
        elements.videoPlayer2.processor.drawOnVideo3 = true;
        document.getElementById("playVideo3").style.backgroundColor = "#c6c7a4";
    }

    if (!stateVideo3) {
        document.getElementById("c5").style.visibility = "hidden";
        document.getElementById("c6").style.visibility = "hidden";
        elements.videoPlayer1.processor.drawOnVideo3 = false;
        elements.videoPlayer2.processor.drawOnVideo3 = false;
        document.getElementById("playVideo3").style.backgroundColor = "#FAF9DC";
    }
}

elements.videoVolume1.on('change', function(v) {
    var vol = Nexus.scale(v, 0, 100, 0.0, 1.0);
    elements.videoPlayer1.video.volume = vol;
})

elements.videoVolume2.on('change', function(v) {
    var vol = Nexus.scale(v, 0, 100, 0.0, 1.0);
    elements.videoPlayer2.video.volume = vol;
})

elements.chromaKeyToggle1.on('change', function(v) {
    if (v && !video1.paused && !elements.videoPlayer1.processor.invert && !elements.videoPlayer1.processor.stackblur && !elements.videoPlayer1.processor.greyscale) {
        document.getElementById("c2").style.visibility = "visible";
        elements.videoPlayer1.processor.chromakey = true;
    } else if (!v && !elements.videoPlayer1.processor.invert && !elements.videoPlayer1.processor.stackblur && !elements.videoPlayer1.processor.greyscale) {
        document.getElementById("c2").style.visibility = "hidden";
        elements.videoPlayer1.processor.chromakey = false;
    }
});

elements.chromaKeyToggle2.on('change', function(v) {
    if (v && !video2.paused && !elements.videoPlayer2.processor.invert && !elements.videoPlayer2.processor.stackblur && !elements.videoPlayer2.processor.greyscale) {
        document.getElementById("c4").style.visibility = "visible";
        elements.videoPlayer2.processor.chromakey = true;
    } else if (!v && !elements.videoPlayer2.processor.invert && !elements.videoPlayer2.processor.stackblur && !elements.videoPlayer2.processor.greyscale) {
        document.getElementById("c4").style.visibility = "hidden";
        elements.videoPlayer2.processor.chromakey = false;
    }
});

elements.chromaDifference1.on('change', function(v) {
    elements.videoPlayer1.processor.difference = v;
})

elements.chromaDifference2.on('change', function(v) {
    elements.videoPlayer2.processor.difference = v;
})

elements.invertToggle1.on('change', function(v) {
    if (v && !video1.paused && !elements.videoPlayer1.processor.chromakey && !elements.videoPlayer1.processor.stackblur && !elements.videoPlayer1.processor.greyscale) {
        document.getElementById("c2").style.visibility = "visible";
        elements.videoPlayer1.processor.invert = true;
    } else if (!v && !elements.videoPlayer1.processor.chromakey && !elements.videoPlayer1.processor.stackblur && !elements.videoPlayer1.processor.greyscale) {
        document.getElementById("c2").style.visibility = "hidden";
        elements.videoPlayer1.processor.invert = false;
    }
})

elements.invertToggle2.on('change', function(v) {
    if (v && !video2.paused && !elements.videoPlayer2.processor.chromakey && !elements.videoPlayer2.processor.stackblur && !elements.videoPlayer2.processor.greyscale) {
        document.getElementById("c4").style.visibility = "visible";
        elements.videoPlayer2.processor.invert = true;
    } else if (!v && !elements.videoPlayer2.processor.chromakey && !elements.videoPlayer2.processor.stackblur && !elements.videoPlayer2.processor.greyscale) {
        document.getElementById("c4").style.visibility = "hidden";
        elements.videoPlayer2.processor.invert = false;
    }
})

elements.stackBlurToggle1.on('change', function(v) {
    if (v && !video1.paused && !elements.videoPlayer1.processor.chromakey && !elements.videoPlayer1.processor.invert && !elements.videoPlayer1.processor.greyscale) {
        document.getElementById("c2").style.visibility = "visible";
        elements.videoPlayer1.processor.stackblur = true;
    } else if (!v && !elements.videoPlayer1.processor.chromakey && !elements.videoPlayer1.processor.invert && !elements.videoPlayer1.processor.greyscale) {
        document.getElementById("c2").style.visibility = "hidden";
        elements.videoPlayer1.processor.stackblur = false;
    }
})

elements.stackBlurToggle2.on('change', function(v) {
    if (v && !video2.paused && !elements.videoPlayer2.processor.chromakey && !elements.videoPlayer2.processor.invert && !elements.videoPlayer2.processor.greyscale) {
        document.getElementById("c4").style.visibility = "visible";
        elements.videoPlayer2.processor.stackblur = true;
    } else if (!v && !elements.videoPlayer2.processor.chromakey && !elements.videoPlayer2.processor.invert && !elements.videoPlayer2.processor.greyscale) {
        document.getElementById("c4").style.visibility = "hidden";
        elements.videoPlayer2.processor.stackblur = false;
    }
})

elements.stackBlurRadius1.on('change', function(v) {
    elements.videoPlayer1.processor.radius = v;
})

elements.stackBlurRadius2.on('change', function(v) {
    elements.videoPlayer2.processor.radius = v;
})

elements.greyScaleToggle1.on('change', function(v) {
    if (v && !video1.paused && !elements.videoPlayer1.processor.chromakey && !elements.videoPlayer1.processor.invert && !elements.videoPlayer1.processor.stackblur) {
        document.getElementById("c2").style.visibility = "visible";
        elements.videoPlayer1.processor.greyscale = true;
    } else if (!v && !elements.videoPlayer1.processor.chromakey && !elements.videoPlayer1.processor.invert && !elements.videoPlayer1.processor.stackblur) {
        document.getElementById("c2").style.visibility = "hidden";
        elements.videoPlayer1.processor.greyscale = false;
    }
})

elements.greyScaleToggle2.on('change', function(v) {
    if (v && !video2.paused && !elements.videoPlayer2.processor.chromakey && !elements.videoPlayer2.processor.invert && !elements.videoPlayer2.processor.stackblur) {
        document.getElementById("c4").style.visibility = "visible";
        elements.videoPlayer2.processor.greyscale = true;
    } else if (!v && !elements.videoPlayer2.processor.chromakey && !elements.videoPlayer2.processor.invert && !elements.videoPlayer2.processor.stackblur) {
        document.getElementById("c4").style.visibility = "hidden";
        elements.videoPlayer2.processor.greyscale = false;
    }
})

elements.wiki.addEventListener("click", function(v) {
    var win = window.open("https://bitbucket.org/arturreich1992/audiovideo_beleg/wiki/Interaktion%20mit%20der%20Anwendung", '_blank');
    win.focus();
});