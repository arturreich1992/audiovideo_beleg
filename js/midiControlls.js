import * as elements from "./Racks.js";
let audioPanelSelected;

const midiRangeFaktor = 0.787;
const biquadFreqRange = 157.482;
const midiQualityFaktor = 7.874015;
const midiGainFaktor = 0.31496;
const midiEchoFaktor = 0.007874;

export function sliderChanged(btnID, midiValue, docElements, audioPanel) {
    switch (sliderIdName(btnID)) {
        case "volume":
            midiValue = midiValue * midiRangeFaktor;
            audioPanel.volume.value = midiValue;
            break;

        case "speed":
            midiValue = midiValue / 64;
            audioPanel.speed.value = midiValue;
            break;

        case "biquad":
            midiValue = midiValue * biquadFreqRange;

            if (document.getElementById('highpass' + docElements).checked) {
                audioPanel.highpass.value = midiValue;
            }

            if (document.getElementById('notch' + docElements).checked) {
                audioPanel.notch.value = midiValue;
            }

            if (document.getElementById('peaking' + docElements).checked) {
                audioPanel.peaking.value = midiValue;
            }

            if (document.getElementById('lowpass' + docElements).checked) {
                audioPanel.lowpass.value = midiValue;
            }

            if (document.getElementById('highshelf' + docElements).checked) {
                audioPanel.highshelf.value = midiValue;
            }

            if (document.getElementById('lowshelf' + docElements).checked) {
                audioPanel.lowshelf.value = midiValue;
            }
            break;

        case "gain":
            midiValue = midiValue * midiGainFaktor;
            audioPanel.gain.value = midiValue;
            break;

        case "quality":
            midiValue = midiValue * midiQualityFaktor;
            audioPanel.quality.value = midiValue;
            break;

        case "echo":
            midiValue = midiValue * midiEchoFaktor;
            audioPanel.echofreq.value = midiValue;
            break;

        case "crossfader":
            elements.audioPanel3.crossfader.value = midiValue * midiRangeFaktor;
            break;
    }
}

export function panelSelected(btnID) {

    switch (buttonIdName(btnID)) {
        case "audioPanel1":
            document.getElementById('heading1').className = "panel-heading panelSelected";
            document.getElementById('heading2').className = "panel-heading";
            audioPanelSelected = "left";
            break;

        case "audiopanel2":
            document.getElementById('heading1').className = "panel-heading";
            document.getElementById('heading2').className = "panel-heading panelSelected";
            audioPanelSelected = "right";
            break;
    }
    return audioPanelSelected;
}

export function buttonPressed(btnID, docElements, audioPanel, audioEffects, audioPanelSelected) {

    switch (buttonIdName(btnID)) {
        case "filterON/OFF":
            if (!audioPanel.filter.state) {
                audioPanel.filter.state = true;
            } else {
                audioPanel.filter.state = false;
            }
            break;

        case "play/pause":
            if (audioPanelSelected == "left") {
                audioEffects.playPause1();
            } else if (audioPanelSelected == "right") {
                audioEffects.playPause2();
            }
            break;

        case "replay":
            if (audioPanelSelected == "left") {
                audioEffects.replay1();
            } else if (audioPanelSelected == "right") {
                audioEffects.replay2();
            }
            break;

        case "highpass":
            document.getElementById('highpass' + docElements).click();
            break;

        case "notch":
            document.getElementById('notch' + docElements).click();
            break;

        case "peaking":
            document.getElementById('peaking' + docElements).click();
            break;

        case "lowpass":
            document.getElementById('lowpass' + docElements).click();
            break;

        case "highshelf":
            document.getElementById('highshelf' + docElements).click();
            break;

        case "lowshelf":
            document.getElementById('lowshelf' + docElements).click();
            break;

        case "echo":
            if (!audioPanel.echoFilter.state) {
                audioPanel.echoFilter.state = true;
            } else {
                audioPanel.echoFilter.state = false;
            }
            break;

        case "bitcrusher":
            if (!audioPanel.bitcrusherFilter.state) {
                audioPanel.bitcrusherFilter.state = true;
            } else {
                audioPanel.bitcrusherFilter.state = false;
            }
            break;

        case "noiceconv":
            if (!audioPanel.noiseConvolver.state) {
                audioPanel.noiseConvolver.state = true;
            } else {
                audioPanel.noiseConvolver.state = false;
            }
            break;
    }
}


function buttonIdName(value) {
    let buttonName;
    switch (value) {

        case 16:
            buttonName = "audioPanel1";
            break;

        case 17:
            buttonName = "audiopanel2";
            break;

        case 18:
            buttonName = "filterON/OFF";
            break;

        case 19:
            buttonName = "play/pause";
            break;

        case 20:
            buttonName = "replay";
            break;

        case 23:
            buttonName = "highpass";
            break;

        case 24:
            buttonName = "notch";
            break;

        case 27:
            buttonName = "peaking";
            break;

        case 28:
            buttonName = "lowpass";
            break;

        case 31:
            buttonName = "highshelf";
            break;

        case 32:
            buttonName = "lowshelf";
            break;

        case 49:
            buttonName = "echo";
            break;

        case 50:
            buttonName = "bitcrusher";
            break;

        case 51:
            buttonName = "noiceconv";
            break;
    }
    return buttonName;
}

function sliderIdName(value) {
    let sliderName;

    switch (value) {

        case 18:
            sliderName = "volume";
            break;

        case 19:
            sliderName = "speed";
            break;

        case 48:
            sliderName = "biquad";
            break;

        case 49:
            sliderName = "gain";
            break;

        case 50:
            sliderName = "quality";
            break;

        case 51:
            sliderName = "echo";
            break;

        case 64:
            sliderName = "crossfader";
            break;
    }
    return sliderName;
}