import * as elements from "./Racks.js";
import * as audioEffects from "./audioEffects.js";
import * as midiControlls from "./midiControlls.js";

let audioPanel1 = elements.audioPanel1;
let audioPanel2 = elements.audioPanel2;

let midiAccess;
let btnID, midiValue, cmd;
let biquadToggle = false;
let audioPanelSelected = "left";

document.addEventListener('DOMContentLoaded', (event) => {
    initMidi();
});

function initMidi() {
    if (navigator.requestMIDIAccess) {
        navigator.requestMIDIAccess().then(
            midiSuccess,
            midiFailure
        );
    } else {
        midiFailure();
    }
}

function midiSuccess(midi) {
    console.log("Midi Success");

    midiAccess = midi;
    var inputs = midi.inputs;
    for (var input of inputs.values()) {
        input.onmidimessage = onMidiMessage;
    }
}

function midiFailure() {
    console.log("Midi failure")
}


function onMidiMessage(event) {
    cmd = event.data[0] >> 4;
    btnID = event.data[1];
    midiValue = event.data[2];

    if (cmdName(cmd) == "buttonPressed") {
        audioPanelSelected = midiControlls.panelSelected(btnID);

        if (audioPanelSelected == "left") {
            midiControlls.buttonPressed(btnID, '1', audioPanel1, audioEffects, "left");
        }
        if (audioPanelSelected == "right") {
            midiControlls.buttonPressed(btnID, '2', audioPanel2, audioEffects, "right");
        }
    }

    if (cmdName(cmd) == "slider") {
        if (audioPanelSelected == "left") {

            midiControlls.sliderChanged(btnID, midiValue, '1', audioPanel1);
        }

        if (audioPanelSelected == "right") {
            midiControlls.sliderChanged(btnID, midiValue, '2', audioPanel2);
        }
    }
}

function cmdName(value) {
    var cmdName;
    switch (value) {
        case 8:
            cmdName = "buttonReleased";
            break;
        case 9:
            cmdName = "buttonPressed";
            break;
        case 11:
            cmdName = "slider";
            break;

    }
  
    return cmdName;
}