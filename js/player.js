export default class Player {
    constructor(audioFile, audioContext) {
        //the HTMLElement audio property
        this.audio = this.createSource(audioFile, audioContext);
        this.isPlaying = false;
        this.isLooping = false;
    }

    createSource(soundFile, audioContext) {
        var audioFile = new Audio(soundFile);
        var source = audioContext.createMediaElementSource(audioFile);
        var gainNode = audioContext.createGain();
        var filter = audioContext.createBiquadFilter();
        filter.type = 'highpass';
        
        var echoFilter = audioContext.createDelay();
        echoFilter.delayTime.value = 0.5;

        var hallFilter = audioContext.createConvolver();

        var noiseConvolver = audioContext.createConvolver();
        var noiseBuffer = audioContext.createBuffer(2, 0.5 * audioContext.sampleRate, audioContext.sampleRate);

        //Bitcrusher
        var bufferSize = 4096;
        var bitcrushFilter = audioContext.createScriptProcessor(bufferSize, 1, 1);

        source.loop = true;
        source.connect(gainNode);
        gainNode.connect(audioContext.destination);
        return {
            audioFile: audioFile,
            source: source,
            gainNode: gainNode,
            filter: filter,
            noiseConvolver: noiseConvolver,
            noiseBuffer: noiseBuffer,
            echoFilter: echoFilter,
            bitcrushFilter: bitcrushFilter
        };
    }
}