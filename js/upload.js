import * as effects from './audioEffects.js';
import * as elements from './Racks.js';

+
function($) {
    'use strict';

    var dropZone = document.getElementById('heading1');
    var dropZone2 = document.getElementById('heading2');
    var dropZone3 = document.getElementById('heading3');
    var dropZone4 = document.getElementById('heading4');

    // Audio Upload
    dropZone.ondragover = function() {
        this.className = 'panel-heading drop';
        return false;
    }

    dropZone.ondragleave = function() {
        this.className = 'panel-heading';
        return false;
    }

    dropZone2.ondragover = function() {
        this.className = 'panel-heading drop';
        return false;
    }

    dropZone2.ondragleave = function() {
        this.className = 'panel-heading';
        return false;
    }

    dropZone.ondrop = function(e) {
        e.stopPropagation();
        e.preventDefault();
        this.className = 'panel-heading';
        var files = e.dataTransfer.files;
        if (files[0].type == "audio/mp3") {
            document.getElementById('audio1Title').innerText = (files[0].name);
            effects.changeAudio(1, URL.createObjectURL(files[0]));
            reset(1);
        }
    }
    dropZone2.ondrop = function(e) {
        e.stopPropagation();
        e.preventDefault();
        this.className = 'panel-heading';
        var files = e.dataTransfer.files;
        if (files[0].type == "audio/mp3") {
            document.getElementById('audio2Title').innerText = (files[0].name);
            effects.changeAudio(2, URL.createObjectURL(files[0]));
            reset(2);
        }
    }

    //Video Upload
    dropZone3.ondragover = function() {
        this.className = 'panel-heading drop';
        return false;
    }

    dropZone3.ondragleave = function() {
        this.className = 'panel-heading';
        return false;
    }

    dropZone4.ondragover = function() {
        this.className = 'panel-heading drop';
        return false;
    }

    dropZone4.ondragleave = function() {
        this.className = 'panel-heading';
        return false;
    }

    dropZone3.ondrop = function(e) {
        e.stopPropagation();
        e.preventDefault();
        this.className = 'panel-heading';
        var files = e.dataTransfer.files;
        if (files[0].name.includes("mpd")) {
            document.getElementById('video1Title').innerText = (files[0].name);
            var player = new dashjs.MediaPlayer().create();
            player.initialize();
            player.attachView(elements.video1);
            player.attachSource("./public/" + files[0].name);
        }
    }

    dropZone4.ondrop = function(e) {
        e.stopPropagation();
        e.preventDefault();
        this.className = 'panel-heading';
        var files = e.dataTransfer.files;
        if (files[0].name.includes("mpd")) {
            document.getElementById('video2Title').innerText = (files[0].name);
            var player = new dashjs.MediaPlayer().create();
            player.initialize();
            player.attachView(elements.video2);
            player.attachSource("./public/" + files[0].name);
        }
    }
}(jQuery);

function reset(panel) {
    if (panel == 1) {
        elements.audioPanel1.volume.value = 50;
        elements.audioPanel1.speed.value = 1;
        elements.audioPanel1.echofreq.value = 0.5;
        elements.audioPanel1.filter.state = false;
        elements.audioPanel1.highpass.value = 440;
        elements.audioPanel1.notch.value = 440;
        elements.audioPanel1.peaking.value = 440;
        elements.audioPanel1.lowpass.value = 440;
        elements.audioPanel1.highshelf.value = 440;
        elements.audioPanel1.lowshelf.value = 440;
        elements.audioPanel1.gain.value = 0.0;
        elements.audioPanel1.quality.value = 1.0;

        elements.audioPanel1.echoFilter.state = false;
        elements.audioPanel1.echofreq.value = 0.5;
        elements.audioPanel1.bitcrusherFilter.state = false;
        elements.audioPanel1.noiseConvolver.state = false;
    } else if (panel == 2) {
        elements.audioPanel2.volume.value = 50;
        elements.audioPanel2.speed.value = 1;
        elements.audioPanel2.echofreq.value = 0.5;
        elements.audioPanel2.filter.state = false;
        elements.audioPanel2.highpass.value = 440;
        elements.audioPanel2.notch.value = 440;
        elements.audioPanel2.peaking.value = 440;
        elements.audioPanel2.lowpass.value = 440;
        elements.audioPanel2.highshelf.value = 440;
        elements.audioPanel2.lowshelf.value = 440;
        elements.audioPanel2.gain.value = 0.0;
        elements.audioPanel2.quality.value = 1.0;

        elements.audioPanel2.echoFilter.state = false;
        elements.audioPanel2.echofreq.value = 0.5;
        elements.audioPanel2.bitcrusherFilter.state = false;
        elements.audioPanel2.noiseConvolver.state = false;
    }
}