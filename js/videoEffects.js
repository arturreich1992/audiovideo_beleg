import * as StackBlur from '../node_modules/stackblur-canvas/dist/stackblur-es.min.js';
export default class Processor {
    constructor(video, c1, c2) {
        this.video = video;
        this.c1 = c1;
        this.c2 = c2;
        this.chromaKeyColors = [2, 204, 26];
        this.difference = 100;
        this.chromakey = false;
        this.drawOnVideo3 = false;
        this.invert = false;
        this.stackblur = false;
        this.radius = 4;
        this.greyscale = false;
    }

    timerCallback() {
        if (this.video.paused || this.video.ended) {
            return;
        }

        if (this.drawOnVideo3) {
            this.drawOnVideo3Canvas();
        }

        if (this.chromakey) {
            this.chromaKeyFilter();
        }

        if (this.invert) {
            this.invertFilter();
        }

        if (this.stackblur) {
            this.stackBlurEffect();
        }

        if (this.greyscale) {
            this.greyScaleFilter();
        }

        let self = this;
        setTimeout(function() {
            self.timerCallback();
        }, 0);
    }

    doLoad() {
        this.ctx1 = this.c1.getContext("2d");
        this.ctx2 = this.c2.getContext("2d");
        this.c1.width = this.video.clientWidth;
        this.c1.height = this.video.clientHeight - 20;
        this.c2.width = this.video.clientWidth;
        this.c2.height = this.video.clientHeight - 20;
        let self = this;
        this.video.addEventListener("play", function() {
            self.width = self.video.videoWidth / 2.55;
            self.height = self.video.videoHeight / 2.55;
            self.timerCallback();
        }, false);
    }

    chromaKeyFilter() {
        this.ctx1.drawImage(this.video, 0, 0, this.width, this.height);
        let frame = this.ctx1.getImageData(0, 0, this.width, this.height);
        let l = frame.data.length / 4;

        for (let i = 0; i < l; i++) {
            let r = frame.data[i * 4 + 0];
            let g = frame.data[i * 4 + 1];
            let b = frame.data[i * 4 + 2];

            //https://en.wikipedia.org/wiki/Color_difference
            var rdiff = r - this.chromaKeyColors[0];
            var gdiff = g - this.chromaKeyColors[1];
            var bdiff = b - this.chromaKeyColors[2];
            var rr = r + this.chromaKeyColors[0];

            var diff = Math.sqrt(2 * Math.pow(rdiff, 2) + 4 * Math.pow(gdiff, 2) + 3 * Math.pow(bdiff, 2) + (rr * (Math.pow(rdiff, 2)) - Math.pow(bdiff, 2)) / 256);

            if (diff < this.difference) {
                frame.data[i * 4 + 3] = 0;
            }
        }

        this.ctx2.putImageData(frame, 0, 0);
        return;
    }

    setColors(colors) {
        this.chromaKeyColors = colors;
        if (this.chromakey) {
            this.chromaKeyFilter();
        }
    }

    drawOnVideo3Canvas() {
        if (!this.video.paused && (this.chromakey || this.invert || this.stackblur || this.greyscale)) {
            if (this.video == document.getElementById("video1")) {
                var c5 = document.getElementById("c5");
                c5.style.visibility = "visible";
                var ctx5 = c5.getContext("2d");

                c5.width = this.c2.width;
                c5.height = this.c2.height;

                var frame1 = this.ctx2.getImageData(0, 0, this.width, this.height);
                ctx5.putImageData(frame1, 0, 0);
            } else if (this.video == document.getElementById("video2")) {
                var c6 = document.getElementById("c6");
                c6.style.visibility = "visible";
                var ctx6 = c6.getContext("2d");

                c6.width = this.c2.width;
                c6.height = this.c2.height;

                var frame2 = this.ctx2.getImageData(0, 0, this.width, this.height);
                ctx6.putImageData(frame2, 0, 0);
            }
        }

        if (!(this.chromakey || this.invert || this.stackblur || this.greyscale)) {
            this.video == document.getElementById("video1") ? document.getElementById("c5").style.visibility = "hidden" : document.getElementById("c6").style.visibility = "hidden";
        }
    }

    invertFilter() {
        this.ctx1.drawImage(this.video, 0, 0, this.width, this.height);
        let frame = this.ctx1.getImageData(0, 0, this.width, this.height);
        let l = frame.data.length / 4;

        for (let i = 0; i < l; i++) {
            frame.data[i * 4 + 0] = 255 - frame.data[i * 4 + 0];
            frame.data[i * 4 + 1] = 255 - frame.data[i * 4 + 1];
            frame.data[i * 4 + 2] = 255 - frame.data[i * 4 + 2];
        }

        this.ctx2.putImageData(frame, 0, 0);
        return;
    }

    stackBlurEffect() {
        this.ctx1.drawImage(this.video, 0, 0, this.width, this.height);
        StackBlur.canvasRGB(this.c1, 0, 0, this.c1.width, this.c1.height, this.radius);
        let frame = this.ctx1.getImageData(0, 0, this.width, this.height);

        this.ctx2.putImageData(frame, 0, 0);
        return;
    }

    greyScaleFilter() { //http://html5doctor.com/video-canvas-magic/
        this.ctx1.drawImage(this.video, 0, 0, this.width, this.height);
        let frame = this.ctx1.getImageData(0, 0, this.width, this.height);
        let l = frame.data.length / 4;

        for (let i = 0; i < l; i++) {
            let r = frame.data[i * 4 + 0];
            let g = frame.data[i * 4 + 1];
            let b = frame.data[i * 4 + 2];

            var brightness = (3 * r + 4 * g + b) >>> 3;
            frame.data[i * 4 + 0] = brightness;
            frame.data[i * 4 + 1] = brightness;
            frame.data[i * 4 + 2] = brightness;
        }

        this.ctx2.putImageData(frame, 0, 0);
        return;
    }
}