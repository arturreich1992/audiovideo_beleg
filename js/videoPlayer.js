import Processor from "./videoEffects.js";
export default class VideoPlayer {
    constructor(video, c1, c2) {
        this.video = video;
        this.c1 = c1;
        this.c2 = c2;
        this.processor = new Processor(this.video, this.c1, this.c2);
        this.processor.doLoad();
    }

    setColors(colors) {
        this.processor.setColors(colors);
    }
}